# Demosessies

Dit project bevat de slides van iedere demosessie met ingang van 9 maart 2022

| Datum        | Link
| ------------ | -------------------
| 9 maart 2022 | [Demosessie Portaal en NLX](./slides/20220309-Virtueel-Inkomsten-Loket-Demo.pdf)
| 20 april 2022 | [Demosessie Portaal, Gemeenten en Samenwerking](./slides/20220420%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 1 juni 2022 | [Demosessie leerervaringen pilot](./slides/20220601%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
